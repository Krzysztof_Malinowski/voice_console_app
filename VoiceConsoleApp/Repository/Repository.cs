﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceConsoleApp.Functions;

namespace VoiceConsoleApp
{

    public static class Repository
    {
        private static readonly string FolderName = "Nagrania";
        private static readonly string FolderNameTest = "Test";

        public static List<User> GetUsers()
        {
            List<User> users = new List<User>();
            string startupPath = System.IO.Directory.GetCurrentDirectory() + "\\" + FolderName;

            DirectoryInfo di = new DirectoryInfo(startupPath);
            DirectoryInfo[] diArr = di.GetDirectories();

            foreach (DirectoryInfo dri in diArr)
            {
                users.Add(new User(dri.Name, dri.FullName));
            }

            return users;
        }

        public static User LoadTestUser(int cepstrumCount, double normalization = 0.0)
        {
            User user = new User("none","none");
            string startupPath = System.IO.Directory.GetCurrentDirectory() + "\\" + FolderNameTest;

            List<Sequence> signals = new List<Sequence>();
            DirectoryInfo di = new DirectoryInfo(startupPath);

            FileInfo[] diArr = di.GetFiles();

            foreach (FileInfo file in diArr)
            {
                var signal = AudioOperations.OpenAudioFile(file.FullName);

                if (normalization > 0.0)
                {
                    signal.Normalization(normalization);
                }

                var signalMFCC = MFCC.ExtractFeatures(signal.data, signal.sampleRate, cepstrumCount);
                signals.Add(signalMFCC);
            }

            user.Signals1 = signals;
            return user;
        }

        public static User LoadTestUserFromFile(int cepstrumCount,string aStartupPath)
        {
            User user = new User("none", "none");

            List<Sequence> signals = new List<Sequence>();

            var signal = AudioOperations.OpenAudioFile(aStartupPath);
            var signalMFCC = MFCC.ExtractFeatures(signal.data, signal.sampleRate, cepstrumCount);
            signals.Add(signalMFCC);

            user.Signals1 = signals;
            return user;
        }

        static public List<String> GetNames(List<User> aUsers)
        {
            List<String> names = new List<string>();

            foreach (var item in aUsers)
            {
                names.Add(item.Name1);
            }
            return names;
        }
    }
}
