﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceConsoleApp.Functions;

namespace VoiceConsoleApp
{
    public class User
    {
        private string Name;
        private List<Sequence> Signals;
        private string Path;

      
        public User(string aName, string aPath)
        {
            this.Name = aName;
            this.Path = aPath;
        }

        public string Name1 { get => Name; set => Name = value; }
        public List<Sequence> Signals1 { get => Signals; set => Signals = value; }
        public string Path1 { get => Path; set => Path = value; }

        public void LoadSignals(int cepstrumCount,double normalization = 0.0 )
        {
            List<Sequence> signals = new List<Sequence>();
            DirectoryInfo di = new DirectoryInfo(this.Path);

            FileInfo[] diArr = di.GetFiles();

            foreach (FileInfo file in diArr)
            {
                AudioSignal signal = AudioOperations.OpenAudioFile(file.FullName);

                if(normalization > 0.0)
                {
                    signal.Normalization(normalization);
                }

                var signalMFCC = MFCC.ExtractFeatures(signal.data, signal.sampleRate, cepstrumCount);
                signals.Add(signalMFCC);
            }

            this.Signals = signals;
        }

        public double GetNormalizationValue(int cepstrumCount)
        {
            double OldValue = 0.0;

            DirectoryInfo di = new DirectoryInfo(this.Path);

            FileInfo[] diArr = di.GetFiles();

            foreach (FileInfo file in diArr)
            {
                var signal = AudioOperations.OpenAudioFile(file.FullName);
                double NewValue = signal.data.Max();
                if (OldValue < NewValue)
                {
                    OldValue = NewValue;
                }
            }

            return OldValue;
        }
    }
}
