﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceConsoleApp.Functions
{
    public class KNN
    {
        private readonly int k;

        public struct Data
        {
            public Data(double distance, string label)
            {
                this.Label = label;
                this.Distance = distance;
            }

            public double Distance { get; set; }
            public string Label { get; set; }
        }

        public KNN(int k)
        {
            this.k = k;
        }

        public String Classification(List<User> users, User user, List<String> aNames)
        {
            //Utworzenie odległości
            List<Data> dataList = CalculateDistance(users, user);

            //Sortowanie
            dataList = GetOrderElements(dataList);

            Dictionary<string, int> labels = new Dictionary<string, int>();
            //Pobranie różnych labels
            foreach (var item in aNames)
            {
                List<Data> temp = dataList.Where(x => x.Label == item).ToList();
                labels.Add(item, temp.Count);
            }
            return labels.OrderBy(x => x.Value).Last().Key;
        }

        private List<Data> CalculateDistance(List<User> users, User aUser)
        {
            List<Data> dataList = new List<Data>();

            foreach (var user in users)
            {
                foreach (var vectors in user.Signals1)
                {
                    var distance = DynamicTimeWarpingOperations.DTW_Distance(aUser.Signals1.Last(), vectors);
                    dataList.Add(new Data(distance,user.Name1));
                }
            }
            return dataList;
        }

        private List<Data> GetOrderElements(List<Data> aData)
        {
            return aData.OrderBy(x => x.Distance).Take(this.k).ToList();
        }

        private List<Data> GetOrderElementsNotZeroDistance(List<Data> aData)
        {
            return aData.OrderBy(x => x.Distance).Where(x => x.Distance > 0).Take(this.k).ToList();
        }

        public (string, bool) ClassificationToTest(List<User> aUsers,User aUser, Sequence vector, List<String> aNames)
        {
            //Utworzenie odległości
            List<Data> dataList = new List<Data>();

            foreach (var user in aUsers)
            {
                foreach (var vectors in user.Signals1)
                {
                    var distance = DynamicTimeWarpingOperations.DTW_Distance(vector, vectors);
                    dataList.Add(new Data(distance, user.Name1));
                }
            }

            //czyszczenie usuniecie wektora o distance =0  + Sortowanie
            dataList = GetOrderElementsNotZeroDistance(dataList);

            Dictionary<string, int> labels = new Dictionary<string, int>();
            //Pobranie różnych labels
            foreach (var item in aNames)
            {
                List<Data> temp = dataList.Where(x => x.Label == item).ToList();
                labels.Add(item, temp.Count);
            }
            string detected = labels.OrderBy(x => x.Value).Last().Key;

            if (aUser.Name1 == detected)
            {
                return (detected, true);
            }
            return (detected, false);
        }
    }
}
