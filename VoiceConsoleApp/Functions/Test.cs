﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceConsoleApp.Functions
{
    class Test
    {
        public List<User> users;
        public User user;
        public int RecognitionGood = 0;
        public int RecognitionBad = 0;

        public int GetAll()
        {
            return RecognitionBad + RecognitionGood;
        }

        public Double GetGoodPercentage()
        {
            var x = GetAll();
            var y = RecognitionGood;
            return (Double)RecognitionGood/(Double)GetAll()*100.0;
        }

        public int GetBadPercentage()
        {
            return RecognitionBad / GetAll() * 100;
        }

        public Test(List<User> aUsers,User aUser)
        {
            users = new List<User>(aUsers);
            user = aUser;
            //this.users.Remove(user);
        }
        public void Start(int k)
        {
            KNN knn = new KNN(k);

            for(int i = 0; i < user.Signals1.Count; i++)
            {
                Sequence vector = user.Signals1[i];
                String detected = "x";
                bool success = false;
                // usunąć testowy wektor
                //Kto pasuje do tego vectora
                (detected,success) = knn.ClassificationToTest(users, user, vector, Repository.GetNames(users));

                if (success)
                {
                    RecognitionGood++;
                }
                else
                {
                    RecognitionBad++;
                }

                Console.WriteLine(user.Name1 + " dla vectora o id = " + i + " pasuje " + detected);
            }
        }

        public void Summary()
        {
            Console.WriteLine();
            Console.WriteLine(user.Name1);
            Console.WriteLine("Good = " + RecognitionGood);
            Console.WriteLine("Bad = " + RecognitionBad);
            Console.WriteLine(GetGoodPercentage() + "%");
            Console.WriteLine("**********************************");
        }
    }
}
