using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceConsoleApp.Functions;
using Accord.DirectSound;
using NAudio.Wave;

namespace VoiceConsoleApp
{
    class Program
    {
        public static WaveInEvent waveSource = null;
        public static WaveFileWriter waveFile = null;
        public static double Normalization = 0.0;

        static void Main(string[] args)
        {
            bool exit = false;
            int capstrum = 13;
            int k = 4;
            List<User> users = PrepareToStart(capstrum);
            User User;
            KNN knn;


            Console.WriteLine("Defaut k = " + k + ", capstrum = " + capstrum);
            CommandLine();

            while (true)
            {
                string valueFromUser = Console.ReadLine();
                switch (valueFromUser)
                {
                    case "help":
                        CommandLine();
                        break;

                    case "shutdown":
                        exit = true;
                        break;

                    case "capstrum":
                        Console.WriteLine("How many ?");
                        string Capstrum = Console.ReadLine();

                        if (Int32.TryParse(Capstrum, out capstrum)){
                            foreach (User user in users)
                            {
                                user.LoadSignals(capstrum);
                            }
                            Console.WriteLine("New Capstrum = " + capstrum);
                        }
                        break;

                    case "k":
                        Console.WriteLine("How many ?");
                        string K = Console.ReadLine();

                        if (Int32.TryParse(K, out k))
                        {
                            Console.WriteLine("New k = " + k);
                        }
                        break;

                    case "test":
                        Console.WriteLine("Test: k = " + k + ", capstrum = " + capstrum);
                        User = Repository.LoadTestUser(capstrum,Normalization);
                        knn = new KNN(k);
                        Console.WriteLine(knn.Classification(users, User, Repository.GetNames(users)));
                        break;

                    case "tests":
                        Console.WriteLine("Test: k = " + k + ", capstrum = " + capstrum);
                        List<Test> tests = new List<Test>();
                        foreach (User user in users)
                        {
                            Test test = new Test(users, user);
                            test.Start(k);
                            test.Summary();
                            tests.Add(test);
                        }

                        double good = 0;
                        double bad = 0;

                        foreach (Test test in tests)
                        {
                            good += test.RecognitionGood;
                            bad += test.RecognitionBad;
                        }
                        double Percentage = good / (good + bad) * 100;
                        Console.WriteLine("W sumie " + Percentage + "%");
         
                        break;

                    case "record":
                        Console.WriteLine("Press a key to star record and again to end");
                        Console.ReadKey();

                        string outputFilePath = System.IO.Directory.GetCurrentDirectory() + "\\system_recorded_audio.wav";

                        waveSource = new WaveInEvent();
                        waveSource.WaveFormat = new WaveFormat(16000, 1);

                        waveSource.DataAvailable += new EventHandler<WaveInEventArgs>(WaveSourceDataAvailable);
                        waveSource.RecordingStopped += new EventHandler<StoppedEventArgs>(WaveSourceRecordingStopped);

                        waveFile = new WaveFileWriter(outputFilePath, waveSource.WaveFormat);

                        waveSource.StartRecording();
                        Console.ReadKey();
                        waveSource.StopRecording();
                        
                        Console.WriteLine("End of recording");
                        Console.ReadKey();
                        Console.WriteLine("Test: k = " + k + ", capstrum = " + capstrum);
                        User = Repository.LoadTestUserFromFile(capstrum, outputFilePath);
                        knn = new KNN(k);
                        Console.WriteLine(knn.Classification(users, User, Repository.GetNames(users)));

                        break;

                    default:
                        //WriteLine("wrong code");
                        break;
                }

                if (exit) break;
            }
        }

        public static void WaveSourceDataAvailable(object sender, WaveInEventArgs e)
        {
            if (waveFile != null)
            {
                waveFile.Write(e.Buffer, 0, e.BytesRecorded);
                waveFile.Flush();
            }
        }

        public static void WaveSourceRecordingStopped(object sender, StoppedEventArgs e)
        {
            if (waveSource != null)
            {
                waveSource.Dispose();
                waveSource = null;
            }

            if (waveFile != null)
            {
                waveFile.Dispose();
                waveFile = null;
            }
        }

        public static List<User> PrepareToStart(int capstrum)
        {
            Console.WriteLine("Wait ...");
            List<User> users = Repository.GetUsers();
            bool norm = false;

            if (norm)
            {
                foreach (User user in users)
                {
                    double NewNormalization = user.GetNormalizationValue(capstrum);

                    if (Normalization < NewNormalization)
                    {
                        Normalization = NewNormalization;
                    }
                }
            }

            return LoadSignals(users,capstrum);
        }

        public static List<User> LoadSignals(List<User> users, int capstrum)
        {
      
            foreach (User user in users)
            {
                user.LoadSignals(capstrum,Normalization);
            }

            return users;
        }

        public static void CommandLine()
        {
            Console.WriteLine("*****************************************************");
            Console.WriteLine("test");
            Console.WriteLine("tests");
            Console.WriteLine("record");
            // Console.WriteLine("capstrum"); bug DTW distance function
            Console.WriteLine("k");
            Console.WriteLine("help");
            Console.WriteLine("shutdown");
            Console.WriteLine("*****************************************************");
        }
    }
}
